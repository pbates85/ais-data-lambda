# Use a Maven base image
FROM maven:3.8.4-openjdk-17 AS build

# Set the working directory in the container
WORKDIR /app

# Copy the pom.xml file to the container
COPY pom.xml .

# Resolve Maven dependencies (this step can be cached if the pom.xml doesn't change)
RUN mvn dependency:go-offline -B

# Copy the rest of the project source code to the container
COPY src ./src

# Build the project
RUN mvn compile dependency:copy-dependencies -DincludeScope=runtime

# Use a Java base image for the final image
FROM public.ecr.aws/lambda/java:11

# Copy the built JAR file from the build stage
COPY --from=build /app/target/classes ${LAMBDA_TASK_ROOT}
COPY --from=build /app/target/dependency/* ${LAMBDA_TASK_ROOT}/lib/

CMD ["org.trident.LambdaHandler::handleRequest"]

