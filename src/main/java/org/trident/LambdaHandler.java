package org.trident;


import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.Arrays;
import java.util.List;

import static org.trident.transformation.Metamorpho.readCSVFile;

public class LambdaHandler implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {
    public static void main(String[] args) {
        System.out.println("JVM go burrr");
    }
    APIGatewayProxyResponseEvent response = new APIGatewayProxyResponseEvent();

    Gson gson = new GsonBuilder().setPrettyPrinting().create();
    @Override
    public APIGatewayProxyResponseEvent handleRequest(APIGatewayProxyRequestEvent event, Context context)
    {
        // Log intake
        LambdaLogger logger = context.getLogger();
        logger.log("ENVIRONMENT VARIABLES: " + gson.toJson(System.getenv()));
        logger.log("CONTEXT: " + gson.toJson(context));
        logger.log("EVENT: " + gson.toJson(event));
        logger.log("EVENT TYPE: " + event.getClass().toString());

        // Create the response object
        APIGatewayProxyResponseEvent response = new APIGatewayProxyResponseEvent();

        try {
            // Get the CSV data from the event body
            String csvData = event.getBody();

            // Print the CSV data
            System.out.println("CSV Data:");
            System.out.println(csvData);

            // Convert CSV data to a list of rows
            List<String> rows = Arrays.asList(csvData.split("\\r?\\n"));

            // Skip the header row
            if (!rows.isEmpty()) {
                rows = rows.subList(1, rows.size());
            }

            // Print each row of the CSV data
            for (String row : rows) {
                System.out.println(row);
            }

            // Use the rows as needed

            response.setStatusCode(200);
            response.setBody("OK");
        } catch (Exception e) {
            response.setStatusCode(500);
            response.setBody("Error: " + e.getMessage());
        }

        return response;
    }
}

//        Exploration obj = new Exploration();
//        obj.listBuckets();
//        //obj.listObjects("US_EAST_2","asidataset");
//        String latestobj = obj.returnLatestKey("asidataset", "");
//        System.out.println("Last uploaded object: " + latestobj);
//
//        File downloadedFile = Exploration.downloadObject("asidataset", latestobj);
//        if (downloadedFile != null) {
//            if (downloadedFile.exists()) {
//                // File already existed and was not downloaded
//            } else {
//                // Newly downloaded file
//            }
//        } else {
//            // Error occurred while downloading the file
//        }
//
//        Metamorpho metamorpho = new Metamorpho();
//        String csvFile = "s3downloads/ais_test_data.csv";
//        metamorpho.printCSVContents(csvFile);
//        try {
//            List<String> rows = metamorpho.readCSVFile(csvFile);
//            metamorpho.printSelectiveValues(rows);
//        } catch (IOException e) {
//            System.err.println("Error reading CSV file: " + e.getMessage());
//        }

