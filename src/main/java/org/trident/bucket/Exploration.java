package org.trident.bucket;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;

public class Exploration {
    private static final String accessKey = System.getenv("aws_access_key_id");
    private static final String secretKey = System.getenv("aws_secret_access_key");
    private static final AmazonS3 s3Client = createS3Client();

    public static void listBuckets() {
        List<Bucket> buckets = s3Client.listBuckets();
        System.out.println("Your {S3} buckets are:");
        for (Bucket b : buckets) {
            System.out.println("* " + b.getName());
        }
    }

    public static void listObjects(String region, String bucketName) {
        System.out.format("Objects in S3 bucket %s:\n", bucketName);
        ListObjectsV2Result result = s3Client.listObjectsV2(bucketName);
        List<S3ObjectSummary> objects = result.getObjectSummaries();
        for (S3ObjectSummary os : objects) {
            System.out.println("* " + os.getKey());
        }
    }

    public static String returnLatestKey(String bucketName, String prefix) {
        AmazonS3 s3Client = createS3Client();
        ListObjectsV2Result result = s3Client.listObjectsV2(bucketName, prefix);
        // Sort the objects based on LastModified timestamp in descending order
        List<S3ObjectSummary> objects = result.getObjectSummaries();
        for (S3ObjectSummary os : objects) {
            System.out.println("Object in bucket: " + bucketName);
            System.out.println("* " + os.getKey());
        }
        objects.sort(Comparator.comparing(S3ObjectSummary::getLastModified).reversed());
        // Get the latest uploaded object (first object in the sorted list)
        if (!objects.isEmpty()) {
            S3ObjectSummary latestObject = objects.get(0);
            return latestObject.getKey();
        } else {
            return null; // No objects found in the bucket.
        }
    }

    public static File downloadObject(String bucketName, String key) {
        String destinationPath = "s3downloads/" + key;
        File destinationFile = new File(destinationPath);

        // Check if the file already exists
        if (destinationFile.exists()) {
            System.out.println("File already exists: " + destinationPath);
            return destinationFile;
        }

        try {
            S3Object s3Object = s3Client.getObject(bucketName, key);
            S3ObjectInputStream inputStream = s3Object.getObjectContent();
            FileOutputStream outputStream = new FileOutputStream(destinationFile);

            byte[] buffer = new byte[1024];
            int bytesRead;
            // method writes the contents of the buffer array to the output stream  Each iteration of the loop fills the buffer array with the next portion of data and writes it to the output stream.
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            } //  returns the number of bytes actually read, or -1 if the end of the stream has been reached.

            outputStream.close();
            inputStream.close();

            System.out.println("Object downloaded successfully to: " + destinationPath);
            return destinationFile;
        } catch (IOException e) {
            System.err.println("Error downloading object from S3: " + e.getMessage());
            return null;
        }
    }


    private static AmazonS3 createS3Client() {
        final BasicAWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);
        return AmazonS3ClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .withRegion("us-east-2")
                .build();
    }
}
