package org.trident.transformation;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Metamorpho {

    public void printCSVContents(String csvFile) {
        BufferedReader reader = null;
        String line;

        try {
            reader = new BufferedReader(new FileReader(csvFile));

            while ((line = reader.readLine()) != null) {
                String[] data = line.split(",");
                for (String item : data) {
                    System.out.print(item + "\t");
                }
                System.out.println();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static List<String> readCSVFile(ByteArrayInputStream inputStream) throws IOException {
        List<String> rows = new ArrayList<>();

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;

            // Skip the header row
            reader.readLine();

            while ((line = reader.readLine()) != null) {
                rows.add(line);
            }
        }

        return rows;
    }

    public static void printSelectiveValues(List<String> rows) {
        System.out.println("MMSI\tVesselName\tLength");

        for (String row : rows) {
            String[] columns = row.split(",");

            // Print the number of columns in each row
            System.out.println("Number of columns: " + columns.length);

            if (columns.length >= 14) { // Adjust index based on the actual number of columns
                String mmsi = columns[0];
                String vesselName = columns[7];
                String length = columns[13]; // Update to index 13 for Length column

                System.out.println(mmsi + "\t" + vesselName + "\t" + length);
            }
        }
    }



}
