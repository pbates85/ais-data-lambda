###   S3   ###
resource "aws_s3_bucket" "lambda_bucket" {
  bucket = var.s3_bucket_name
}
# making the s3 bucket private as it houses the lambda code:
resource "aws_s3_bucket_acl" "lambda_bucket_acl" {
  bucket = aws_s3_bucket.lambda_bucket.id
  acl    = "private"
  depends_on = [aws_s3_bucket_ownership_controls.s3_bucket_acl_ownership]
}

# Resource to avoid error "AccessControlListNotSupported: The bucket does not allow ACLs"
resource "aws_s3_bucket_ownership_controls" "s3_bucket_acl_ownership" {
  bucket = aws_s3_bucket.lambda_bucket.id
  rule {
    object_ownership = "ObjectWriter"
  }
}

###   Lambda Function   ###
resource "aws_lambda_function" "lambda_function" {
  function_name    = var.lambda_function_name
  role             = aws_iam_role.lambda_execution_role.arn
  ##################
  # Container Image
  ##################
  image_uri        = var.LAMBDA_IMAGE_URI
  package_type     = var.LAMBDA_PACKAGE_TYPE
  architectures    = ["x86_64"]
  timeout          = 240
  vpc_config {
    subnet_ids = ["subnet-0922235cf06f974a4"]
    security_group_ids = ["sg-031e257046fd02892"]
  }
  environment {
    variables = {
      aws_access_key_id     = var.AWS_ACCESS_KEY_ID
      aws_secret_access_key = var.AWS_SECRET_ACCESS_KEY
    }
  }
}

#resource "aws_lambda_function" "lambda_function" {
#  function_name    = var.lambda_function_name
#  s3_bucket        = aws_s3_bucket.lambda_bucket.id
#  s3_key           = aws_s3_object.lambda_code.key
#  handler          = "org.example.HelloWorld::handler"
#  runtime          = "java11"
#  source_code_hash = data.archive_file.lambda_code.output_base64sha256
#  role             = aws_iam_role.lambda_execution_role.arn
#}


###   CloudWatch   ###
resource "aws_cloudwatch_log_group" "lambda_log_group" {
  name              = "/aws/lambda/${aws_lambda_function.lambda_function.function_name}"
  retention_in_days = 30
}

###   IAM   ###
resource "aws_iam_role" "lambda_execution_role" {
  name = "lambda_execution_role_${var.lambda_function_name}"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [{
      Action = "sts:AssumeRole"
      Effect = "Allow"
      Sid    = ""
      Principal = {
        Service = "lambda.amazonaws.com"
      }
    }
    ]
  })
}

resource "aws_iam_role" "lambda_vpc_link_role" {
  name = "lambda_vpc_link_role_${var.lambda_function_name}"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        "Version": "2012-10-17",
        "Statement": [
          {
            "Action": "ec2:*",
            "Effect": "Allow",
            "Resource": "*"
          },
          {
            "Effect": "Allow",
            "Action": "elasticloadbalancing:*",
            "Resource": "*"
          },
          {
            "Effect": "Allow",
            "Action": "cloudwatch:*",
            "Resource": "*"
          },
          {
            "Effect": "Allow",
            "Action": "autoscaling:*",
            "Resource": "*"
          },
          {
            "Effect": "Allow",
            "Action": "iam:CreateServiceLinkedRole",
            "Resource": "*",
            "Condition": {
              "StringEquals": {
                "iam:AWSServiceName": [
                  "autoscaling.amazonaws.com",
                  "ec2scheduled.amazonaws.com",
                  "elasticloadbalancing.amazonaws.com",
                  "spot.amazonaws.com",
                  "spotfleet.amazonaws.com",
                  "transitgateway.amazonaws.com"
                ]
              }
            }
          }
        ]
      }
    ]
  })
}


resource "aws_iam_role_policy_attachment" "lambda_policy" {
  role       = aws_iam_role.lambda_execution_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

//using archive_file data source to zip the lambda code:
#data "archive_file" "lambda_code" {
#  type        = "zip"
#  source_dir  = "${path.module}/function_code"
#  output_path = "${path.module}/function_code.zip"
#}

#resource "aws_s3_object" "lambda_code" {
#  bucket = aws_s3_bucket.lambda_bucket.id
#  key    = "function_code.zip"
#  source = data.archive_file.lambda_code.output_path
#  etag   = filemd5(data.archive_file.lambda_code.output_path)
#}