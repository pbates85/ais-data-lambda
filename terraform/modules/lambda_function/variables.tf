variable "s3_bucket_name" {
  type        = string
  description = "The name of the S3 bucket to store the Lambda function code"
  default     = "terraform-api-gateway-lambda-demo-161660187510"
}

variable "lambda_function_name" {
  type        = string
  description = "The name of the Lambda function"
  default     = "CommissionETL"
}

variable "LAMBDA_IMAGE_URI" {
  description = "URI of the Lambda function image"
  default = "161660187510.dkr.ecr.us-east-2.amazonaws.com/ais-data-repo:latest"
}

variable "LAMBDA_RUNTIME" {
  description = "Runtime for the Lambda function"
  default = "java11"
}

variable "LAMBDA_PACKAGE_TYPE" {
  description = "Package type for the Lambda function"
  default = "Image"
}

variable "AWS_ACCESS_KEY_ID" {
  description = "AWS access key ID"
  default = ""
}

variable "AWS_SECRET_ACCESS_KEY" {
  description = "AWS secret access key"
  default = ""
}